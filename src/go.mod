module grafana-matrix-forwarder

go 1.15

require (
	github.com/prometheus/client_golang v1.11.0
	maunium.net/go/mautrix v0.7.13
)
